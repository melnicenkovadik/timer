import React, {useEffect, useRef, useState} from 'react'
import './App.scss';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

function App() {
  const [timerDays, setTimerDays] = useState('00')
  const [timerHours, setTimerHours] = useState('00')
  const [timerMinutes, setTimerMinutes] = useState('00')
  const [timerSeconds, setTimerSeconds] = useState('00')
  const [selectedDay, setSelectedDay] = useState(0)
  let interval = useRef(selectedDay)
  const startTimer = () => {
    const CountDownDate = new Date(selectedDay).getTime()
    interval = setInterval(() => {
      const now = new Date().getTime()
      const distance = CountDownDate - now

      const days = Math.floor(distance / (1000 * 60 * 60 * 24))
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)))
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
      const seconds = Math.floor((distance % (1000 * 60)) / 1000)

      if (distance < 0) {
        //stop timer
        clearInterval(interval.current)
      } else {
        //updateTimer
        setTimerDays(days);
        setTimerHours(hours)
        setTimerMinutes(minutes)
        setTimerSeconds(seconds)
      }

    }, 1000)
  }

  useEffect(() => {

    startTimer()

      // eslint-disable-next-line react-hooks/exhaustive-deps
      clearInterval(interval.current)

  },[selectedDay])
  let handleDayChange = (day, {sunday, disabled}) => {
    setSelectedDay(day)
  }

  return (
    <>
      <div className='pick-date'>
        {selectedDay ? (
          <p style={{color: "white"}}>Ви обрали {selectedDay.toLocaleDateString()}</p>
        ) : (
          <p style={{color: "white"}}>Будь ласка, оберiть день:</p>
        )}
        <DayPickerInput
          onDayChange={handleDayChange}/>
      </div>

      <section className='timer-container'>

        <section className='timer'>
          <div>
            <h2>Будь готовий до важливого моменту</h2>
          </div>
          <div>
            <section>
              <p>{timerDays}</p>
              <p><small>Дiб</small></p>
            </section>
            <span>:</span>
            <section>
              <p>{timerHours}</p>
              <p><small>Годин</small></p>
            </section>
            <span>:</span>
            <section>
              <p>{timerMinutes}</p>
              <p><small>Хвилин</small></p>
            </section>
            <span>:</span>
            <section>
              <p>{timerSeconds}</p>
              <p><small>Секунд</small></p>
            </section>
          </div>
        </section>

      </section>

    </>
  );
}

export default App;
